import React from 'react';
import './style.css';
import Errors from "./assets/notfound.jpg";
import Login from './Page/login';
import Home from './Page/home';
import Navigasi from './navigasi';
import RecipeGallery from './Page/recipe.js';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Notify from './Page/notify';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component = {Login} />
        <Route path="/home" exact component = {Home} />
        <Route path="/recipe" exact component = {RecipeGallery} />
        <Route path="/notify" exact component = {Notify} />
      </Switch>
      <Navigasi />
      <PageError />
    </BrowserRouter>
    
  );
}
class PageError extends React.Component{
  render(){
    return(
      <div className="pageError">
        <img src= {Errors}/>
        <h1>We are sorry to tell you</h1>
        <p>You should using Device with 360px screen width.</p>
      </div>
    )
  }
}

export default App;
