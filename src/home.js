import React from 'react';
import headerimage from './assets/woman.png';
import {Link} from 'react-router-dom';

class Home extends React.Component{
    render(){
        return(
            <>
            <div className="HomePage">
                <header>
                    <h1>Hi, Rizki Ramadhan</h1>
                    <p>We have some special Menu today, let's see ...</p>
                    <section className="headerpicture">
                        <img alt="header-img" src={headerimage} />
                        <button>Daily Recipe's</button>
                    </section>
                </header>
                <main>
                    <div className="maincontent">
                        <h1>Your Favourite's</h1>
                        <content className="itemfood">
                            <div className="boxfood">
                                <img alt="food-item" src="https://asset.kompas.com/crops/I9YfYuGPYoqtkGOgVDurgj-2MiY=/0x0:1000x667/750x500/data/photo/2020/06/02/5ed5f39c7d8fe.jpg" />
                                <p>Bakso</p>
                                <button>Get Recipe</button>
                            </div>
                            <div className="boxfood">
                                <img alt="food-item" src="https://media.perjalanandunia.com/wp-content/uploads/2019/01/02142952/rendang-daging_20180405_235157.jpg" />
                                <p>Rendang</p>
                                <button>Get Recipe</button>
                            </div>
                            <div className="boxfood">
                                <img alt="food-item" src="https://resepmembuat.com/wp-content/uploads/2017/05/21.-resep-soto-babat-lamongan.jpg" />
                                <p>Soto Babat</p>
                                <button>Get Recipe</button>
                            </div>
                        </content>
                    </div>
                    <div className="home1stContent">
                        <h1>Wanna Find More?</h1>
                        <content className="boxaction">
                            <img alt="illustration" src="https://image.freepik.com/free-vector/people-cooking-illustration-concept_23-2148533744.jpg" />
                            <div>
                            <Link to="/recipe"><button className="recipeact"><i className="fa fa-search fa-2x"></i>Find Recipe</button></Link>
                            <button className="recipeact"><i className="fa fa-plus fa-2x"></i>Add Recipe</button>
                        </div>
                        </content>
                        
                    </div>
                </main>
            </div>
            </>
        )
    }
}

export default Home;