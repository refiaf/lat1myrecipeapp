import React from 'react';
import logo from './icon.png';
import {Link} from 'react-router-dom';

function Login (){
    return(
        <div className="LoginPage">
            <figure>
                <img alt="Login Icon" className="loginIcon" src={logo}/>
                <figcaption>My Recipe</figcaption>
                <p>Every recipe was Great</p>
            </figure>
            <div className="form-login">
                <form>
                    <label className="labels">
                        <input type="text" required placeholder="Enter your Username"/>
                        <p>Username / Email </p>
                    </label>
                    <label className="labels">
                        <input type="password" required placeholder="Enter your Password" />
                        <p>Password </p>
                    </label>
                    <Link to="/home"><input type="submit" value="Log In" className="login" /></Link>
                </form>
            </div>
            <div className="userInfo">
                <p>Don't have Account? | <a href="http://www.google.com">Register</a></p>
                <p>or Forgot Password? Click here!</p>
            </div>
        </div>
    )
}

export default Login