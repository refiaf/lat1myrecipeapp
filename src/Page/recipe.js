import React, { useState, useEffect } from 'react';
import axios from 'axios';

const RecipeGallery = () => {
    const [listrecipe, setlistrecipe] = useState(null)
    useEffect(() => {
        if(listrecipe === null){
            axios.get(`https://my-json-server.typicode.com/korizki/db/meal`)
            .then(res => {
            setlistrecipe(res.data)
            console.log(res.data)
        })
    }    
    }, [listrecipe]);

    return(
        <>
        <div className="RecipePage">
            <section className="find">
                <h1>Find Recipe</h1>
                <label>
                    <input type="text" placeholder="Enter keyword" className="inputSearch"/>
                    <button id="click"><i className="fa fa-search fa-2x iconcari"></i></button>
                </label>
            </section>
            <section className="result">
                <p id="value">Result of search </p>
                {
                listrecipe !== null && listrecipe.map((item, index) => 
                    <div key={index} className="boxresult">
                        <img title={item.strMeal} className="image" src={item.strMealThumb} alt="fooditem" />
                        <p>{item.strMeal}</p>
                        <a href="http://www.google.com">Get Recipe</a>
                    </div>
                    )
                }
            </section>
        </div>
        </>
    )
}

export default RecipeGallery;